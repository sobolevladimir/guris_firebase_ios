//
//  LogInVC.swift
//  GurisFirebase
//
//  Created by Andrey Plygun on 12/14/20.
//  Copyright © 2020 robosoft guris. All rights reserved.
//

import UIKit

class LogInVC: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationStyle = .fullScreen
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTapped)))
        activityIndicator.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        autoFillTextFields()
        #if DEBUG
            loginTextField.text = "demo@guris.com.tr"
            passwordTextField.text = "123456Aa"
        #endif
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let isLoggedIn = UserDefaults.standard.bool(forKey: UserDefaultsKeys.isLoggedIn)
        if isLoggedIn {
            loginButtonTapped()
        }
    }

    @IBAction func loginButtonTapped() {
        activityIndicator.isHidden = false
        API.loginWith(userName: loginTextField.text!, password: passwordTextField.text!) { [weak self] (response, errorMsg) in
            self?.activityIndicator.isHidden = true
            if let errorMsg = errorMsg {
                self?.showAlertVC(title: "", description: errorMsg, animated: true)
                return
            }
            if let success = response, success {
                UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isLoggedIn)
                UserDefaults.standard.set(self?.loginTextField.text!, forKey: UserDefaultsKeys.login)
                UserDefaults.standard.set(self?.passwordTextField.text!, forKey: UserDefaultsKeys.password)
                self?.showWebViewVC()
            }
        }
    }
    
    private func autoFillTextFields() {
        guard let login = UserDefaults.standard.string(forKey: UserDefaultsKeys.login), let password = UserDefaults.standard.string(forKey: UserDefaultsKeys.password) else { return }
        loginTextField.text = login
        passwordTextField.text = password
    }
    
    private func showWebViewVC() {
        let mapURLString = "https://gurisenerji.robosoftenerji.com/signin?"
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        vc.mapURLString = mapURLString + "username=\(loginTextField.text!)&password=\(passwordTextField.text!)"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if view.frame.origin.y == 0 {
            self.view.frame.origin.y -= 100
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func viewDidTapped() {
        view.endEditing(true)
    }
}
