//
//  WebViewVC.swift
//  GurisFirebase
//
//  Created by Andrey Plygun on 12/14/20.
//  Copyright © 2020 robosoft guris. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var closeMenuButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var callback: (() -> ())?
    var mapURLString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.alpha = 0
        if #available(iOS 11.0, *) {
            self.webView.scrollView.contentInsetAdjustmentBehavior = .never
        }
        webView.load(URLRequest(url: URL(string: mapURLString)!))
        menuView.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        closeMenuButton.imageView?.transform = CGAffineTransform(rotationAngle: .pi)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.7, delay: 4, animations: {
            self.webView.alpha = 1
        }) { (_) in
            self.activityIndicator.isHidden = true
        }
        
//        guard let login = UserDefaults.standard.value(forKey: UserDefaultsKeys.login) as? String,
//            let password = UserDefaults.standard.value(forKey: UserDefaultsKeys.password) as? String else {
//                self.logOut()
//                self.callback?()
//                return
//        }
//        API.loginWith(userName: login, password: password) { (success, error) in
//            if let error = error {
//                self.showAlertVC(title: "", description: error, animated: true)
//                self.logOut()
//                return
//            }
//            if !success.0! {
//                self.logOut()
//                self.callback?()
//            }
//        }
    }
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.25) {
            self.menuView.frame.origin.x -= 200
        }
    }
    
    @IBAction func hideMenuButtonTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.25) {
            self.menuView.frame.origin.x += 200
        }
    }
    
    @IBAction func logOutButtonTapped(_ sender: Any) {
        let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.logOut()
        }
        let noAction = UIAlertAction(title: "No", style: .default, handler: nil)
        self.showAlertVC(title: "", description: "Are you sure you want to log out?", animated: true, actions: [yesAction, noAction])
    }
    
    private func logOut() {
        UserDefaults.standard.set(false, forKey: UserDefaultsKeys.isLoggedIn)
        navigationController?.popViewController(animated: true)
    }
}
