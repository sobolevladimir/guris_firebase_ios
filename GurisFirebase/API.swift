//
//  API.swift
//  GurisFirebase
//
//  Created by Andrey Plygun on 12/14/20.
//  Copyright © 2020 robosoft guris. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct UserDefaultsKeys {
    static let login = "login"
    static let password = "password"
    static let isLoggedIn = "isLoggedIn"
    static let token = "token"
}

class API {
    
    private static let baseURL = "https://gurismobile.robosoftenerji.com/api/api.php?"
//    private static let baseURL = "https://gurisenerji.robosoftenerji.com/signin?"
    private static let loginRoute = "operation=login&"
    
    static func loginWith(userName: String, password: String, callback: @escaping(_ success: Bool?, _ errorMsg: String?) -> ()) {
        let url = URL(string: baseURL + loginRoute + "username=\(userName)&password=\(password)")
        Alamofire.request(url!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).response { (response) in
            if let error = response.error {
                callback(nil, error.localizedDescription)
                return
            }
            if let data = response.data {
                let json = JSON(data)
//                print(json)
                if let success = json["message"].string, success == "success" {
                    callback(true, nil)
                } else {
                    callback(nil, json["message"].string)
                }
            }
        }
    }
}
